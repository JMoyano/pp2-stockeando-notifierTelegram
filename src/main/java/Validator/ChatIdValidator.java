package Validator;

import pp2.stockeando.validators.Validator;

import java.util.regex.Pattern;

public class ChatIdValidator extends Validator {

    private static final Pattern  chatIdValidatorPattern = Pattern.compile("^\\d+$");

    public static boolean isChatIdNumberValid(String number){
        return chatIdValidatorPattern.matcher(number).matches();
    }

}
