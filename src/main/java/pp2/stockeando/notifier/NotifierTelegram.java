package pp2.stockeando.notifier;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import Validator.ChatIdValidator;
import pp2.stockeando.model.UserContact;

public class NotifierTelegram implements Notifier{

	private static final String url = "https://api.telegram.org/bot%s/sendMessage?chat_id=%s&text=%s";
	private static final String apiToken = "2054133278:AAHqnDRvLBdNf7dD2xFT0-7c5pbIFy_OjmA";
	
	
	@SuppressWarnings("unused")
	@Override
	public boolean send(String message, UserContact receiver) {
		  
	       String chatId = receiver.getContactDetail();
	       
	       String[] recorteMensaje = message.split("\n");
			 
			for (int i = 0; i < recorteMensaje.length; i++) {
				String urlString = String.format(url, apiToken, chatId, recorteMensaje[i]);
		        try {
		            URL url = new URL(urlString);
		            URLConnection conn = url.openConnection();
		            InputStream is = new BufferedInputStream(conn.getInputStream());
		           
		        } catch (IOException e) {
		            e.printStackTrace();
		            return false;
		       }
			} 
			return true; 
	}

	@Override
	public String getNameNotifier() {
		return "Telegram";
	}

	@Override
	public boolean isReceiverInfoValid(UserContact receiver) {
		return ChatIdValidator.isChatIdNumberValid(receiver.getContactDetail());
	}
	@Override
	public int compareTo(Notifier o) {
		return this.getNameNotifier().compareTo(o.getNameNotifier());
	}

}
